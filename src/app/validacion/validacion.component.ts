import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-validacion',
  templateUrl: './validacion.component.html',
  styleUrls: ['./validacion.component.scss']
})
export class ValidacionComponent implements OnInit {

  date = new FormControl(new Date());
  constructor(private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  validateAmoshi() {
    const now = new Date();
    if (this.date.value.getDate() === 19 &&
      this.date.value.getMonth() === 3 &&
      this.date.value.getFullYear() === 2018) {
      localStorage.setItem('amoshiHash', btoa('1932018-' + now.getDate() + now.getMonth() + now.getFullYear()));
      this.router.navigate(['mundo-amoshi']);
    } else {
      this.snackBar.open('Aaaaamooooshiiiii 🤔🤔🤔😒😒😒😒😒😒', null, { horizontalPosition: 'center', duration: 5000 });
    }
  }
}

export interface IAmoshiAuth {
  verificationDate: Date;
}
