import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatToolbarModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatButtonModule,
  MAT_DATE_LOCALE,
  MatSnackBarModule,
  MatGridListModule,
  MatIconModule,
} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ValidacionComponent } from './validacion/validacion.component';
import { MundoAmoshiComponent } from './mundo-amoshi/mundo-amoshi.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListaMesesariosComponent } from './mundo-amoshi/lista-mesesarios/lista-mesesarios.component';
import { MesesarioComponent } from './mundo-amoshi/mesesario/mesesario.component';
import { BucketListComponent } from './mundo-amoshi/bucket-list/bucket-list.component';
import { DedicatoriasComponent } from './mundo-amoshi/dedicatorias/dedicatorias.component';
import { NuestrasNotitasComponent } from './mundo-amoshi/nuestras-notitas/nuestras-notitas.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    ValidacionComponent,
    MundoAmoshiComponent,
    ListaMesesariosComponent,
    MesesarioComponent,
    BucketListComponent,
    DedicatoriasComponent,
    NuestrasNotitasComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    AppRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatGridListModule,
    MatIconModule,
    HttpClientModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
