import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DedicatoriasComponent } from './dedicatorias.component';

describe('DedicatoriasComponent', () => {
  let component: DedicatoriasComponent;
  let fixture: ComponentFixture<DedicatoriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DedicatoriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DedicatoriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
