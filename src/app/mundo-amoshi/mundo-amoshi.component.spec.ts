import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MundoAmoshiComponent } from './mundo-amoshi.component';

describe('MundoAmoshiComponent', () => {
  let component: MundoAmoshiComponent;
  let fixture: ComponentFixture<MundoAmoshiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MundoAmoshiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MundoAmoshiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
