import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ListaMesesariosService } from './lista-mesesarios.service';

@Component({
  selector: 'app-lista-mesesarios',
  templateUrl: './lista-mesesarios.component.html',
  styleUrls: ['./lista-mesesarios.component.scss']
})
export class ListaMesesariosComponent implements OnInit {
  mesActual: number;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private mesesarioService: ListaMesesariosService) {
    this.activatedRoute.params.subscribe((params) => {
      this.mesActual = params['numeromes'];
      this.mesesarioService.getMesesario(Number(this.mesActual)).subscribe((response) => {
        console.log(response);
      });
    });
  }

  ngOnInit() {

  }
  irMesesario(numero, signo) {
    console.log(numero);
    if (signo === '+') {
      numero = Number(numero) + 1;
    } else if (signo === '-') {
      numero = Number(numero) - 1;
    }

    if (numero >= 0 && numero <= 11) {
      this.router.navigate(['/lista-mesesarios/' + numero]);
    }
  }
}
