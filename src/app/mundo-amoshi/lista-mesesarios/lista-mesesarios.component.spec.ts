import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMesesariosComponent } from './lista-mesesarios.component';

describe('ListaMesesariosComponent', () => {
  let component: ListaMesesariosComponent;
  let fixture: ComponentFixture<ListaMesesariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMesesariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMesesariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
