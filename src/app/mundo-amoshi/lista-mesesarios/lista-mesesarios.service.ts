import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constant } from 'src/app/constants';


@Injectable({
  providedIn: 'root'
})
export class ListaMesesariosService {

  constructor(private http: HttpClient) {

  }
  getMesesario(number) {
    console.log(Constant.BACKEND_URL);
    return this.http.get(Constant.BACKEND_URL + '/getMesesario/' + number);
  }
}
