import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class BucketListService {

  constructor(private http: HttpClient) { }

  listAllPending() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.http.get(environment.BACKEND_URL + '/bucket/pending', { headers: headers });
  }
  listAllCompleted() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return this.http.get(environment.BACKEND_URL + '/bucket/completed', { headers: headers });
  }
}
