import { Component, OnInit } from '@angular/core';
import { BucketListService } from './bucket-list.service';

@Component({
  selector: 'app-bucket-list',
  templateUrl: './bucket-list.component.html',
  styleUrls: ['./bucket-list.component.scss'],
  providers: [BucketListService]
})
export class BucketListComponent implements OnInit {
   bucketListPending;
   bucketListCompleted;
  constructor(private bucketListService: BucketListService) { }

  ngOnInit() {
    this.bucketListService.listAllCompleted().subscribe((result) => {
      console.log('completed');
      console.table(result);
      this.bucketListCompleted = result;
    });
    this.bucketListService.listAllPending().subscribe((result) => {
      console.log('pending');
      console.table(result);
      this.bucketListPending = result;
    });
  }

}
