import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuestrasNotitasComponent } from './nuestras-notitas.component';

describe('NuestrasNotitasComponent', () => {
  let component: NuestrasNotitasComponent;
  let fixture: ComponentFixture<NuestrasNotitasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuestrasNotitasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuestrasNotitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
