import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesesarioComponent } from './mesesario.component';

describe('MesesarioComponent', () => {
  let component: MesesarioComponent;
  let fixture: ComponentFixture<MesesarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesesarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesesarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
