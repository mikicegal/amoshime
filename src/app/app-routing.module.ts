import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ValidacionComponent } from './validacion/validacion.component';
import { MundoAmoshiComponent } from './mundo-amoshi/mundo-amoshi.component';
import { AuthGuard } from './guard/auth.guard';
import { ListaMesesariosComponent } from './mundo-amoshi/lista-mesesarios/lista-mesesarios.component';
import { BucketListComponent } from './mundo-amoshi/bucket-list/bucket-list.component';



const routes: Routes = [
  { path: '', redirectTo: 'validacion', pathMatch: 'full' },
  { path: 'validacion', component: ValidacionComponent },
  { path: 'mundo-amoshi', component: MundoAmoshiComponent, canActivate: [AuthGuard] },
  { path: 'lista-mesesarios/:numeromes', component: ListaMesesariosComponent, canActivate: [AuthGuard] },
  { path: 'bucket-list', component: BucketListComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
