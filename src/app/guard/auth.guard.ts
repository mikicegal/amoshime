import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private snackBar: MatSnackBar) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const validation = localStorage.getItem('amoshiHash');
    if (validation) {
      const now = new Date();
      const compareString = atob(validation) === '1932018' + '-' + now.getDate() + now.getMonth() + now.getFullYear();
      console.log(validation, atob(validation));
      if (!compareString) {
        this.snackBar.open('Oops!, no puedes entrar aqui por el momento!!! tienes que renovar la validacion :3', '', { duration: 5000 });
      } else {
        this.snackBar.open('Bienvenida Amoshiiii:3  🧙‍♀️ ❤️🧙', '', { duration: 5000 });
      }
      return compareString;

    } else {
      this.router.navigate(['validacion']);
      this.snackBar.open('Oops!, no puedes entrar aqui por el momento!!! tienes que pasar la validacion :3', '', { duration: 5000 });
      return false;
    }

  }
}
